(setq solve-maze
  (letrec
    ((explore-maze
      (lambda (structure agenda end-node)
        (cond
          ((empty-agenda? agenda)
           (throw (quote failed) nil))
          ((eq? end-node (path-destination (first-agenda agenda)))
           (first-agenda agenda))
          ((explore-maze
              structure
              (schedule-agenda
                (rest-agenda agenda)
                (fork-path structure (first-agenda agenda)))
              end-node)))))
      (fork-path
        (lambda (structure path)
          (if (visited-place-twice? (place-list path))
              nil
              (mapcar
                (lambda (place) (append-place place path))
                (reachable-places structure (path-destination path))))))
      (visited-place-twice?
        (lambda (places)
          (member? (first places) (rest places))))
      (reachable-places
        (lambda (structure place)
          (let
            ((edges (assoc place structure)))
            (if (null? edges)
                nil
                (rest edges)))))
      (the-empty-path (lambda () nil))
      (append-place cons)
      (path-destination first)
      (place-list identity)
      (place-list-in-order reverse)
      (empty-agenda? null?)
      (first-agenda first)
      (rest-agenda rest)
      (schedule-agenda append)
      (make-agenda
        (lambda (place)
          (cons
            (append-place place (the-empty-path))
            nil))))
    (lambda (structure start-node end-node)
      (if (eq? start-node end-node)
        (the-empty-path)
        (place-list-in-order
          (explore-maze structure
            (make-agenda start-node)
            end-node))))))
