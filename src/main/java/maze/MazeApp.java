/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze;
/*
 * Created on 23.01.2005
 */
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import maze.controller.MazeController;
import maze.model.MazeModel;
import maze.view.MazePanel;
/**
 * @author Andreasm
 */
public class MazeApp
{
    private static final int WIDTH = 15;
    private static final int HEIGHT = 15;
    private static final int SCALE = 18;
    //
	private MazeApp()
	{
	}
	//
	public static void main(String[] args)
	{
        SwingUtilities.invokeLater(() ->
        {
            try
            {
                var model = new MazeModel(WIDTH, HEIGHT, SCALE);
                var panel = new MazePanel(model, WIDTH, HEIGHT, SCALE);
                var frame = new JFrame("Maze");
                frame.add(panel);
                frame.setResizable(false);
                frame.pack();
                var mazecontroller = new MazeController(model, frame, panel);
                panel.connect();
                mazecontroller.connect();
                frame.setVisible(true);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        });
	}
}