/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.view;
/*
 * Created on 23.01.2005
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import javax.swing.JPanel;
import maze.model.MazeModel;
import maze.model.MazeModelListener;
import maze.infrastructure.Context;
import maze.infrastructure.Orientation;
/**
 * @author Andreasm
 */
public final class MazePanel extends JPanel implements Context, MazeModelListener
{
    private static final long DELAY = 100L;
    //
    private final MazeModel model;
    private final int width;
    private final int height;
    private final int scale;
    private final Dimension size;
    private Image backgroundimage;
    private Image bufferimage;
    private final Ghost ghost;
    //
    public MazePanel(MazeModel model, int width, int height, int scale)
    {
        super();
        //
        this.model = model;
        this.width = width;
        this.height = height;
        this.scale = scale;
        //
        int pixelwidth = (2 * width + 1) * scale;
        int pixelheight = (2 * height + 1) * scale;
        size = new Dimension(pixelwidth, pixelheight);
        ghost = new Ghost(this, Ghost.BLINKY, Orientation.DOWN);
        setBackground(Color.WHITE);
        setOpaque(true);
        setDoubleBuffered(false);
    }
    //
    public void connect()
    {
        model.addListener(this);
    }
    //
    public void mazeChanged()
    {
        var handler = model.getHandler();
        handler.movementEnded(null, ghost);
        backgroundimage = null;
        repaint(300L);
    }
    //
    public Dimension getMinimumSize()
    {
        return size;
    }
    //
    public Dimension getPreferredSize()
    {
        return size;
    }
    //
    public void paintComponent(Graphics graphics)
    {
        var maze = model.getMaze();
        //
        if (maze == null) return;
        //
        if (backgroundimage == null)
        {
            int w = 1 + (width << 1);
            int h = 1 + (height << 1);
            var walls = new boolean[w][h];
            //
            for (int x = 0; x < w; x += 2)
            {
                for (int y = 0; y < h; y++)
                {
                    walls[x][y] = true;
                }
            }
            //
            for (int y = 0; y < h; y += 2)
            {
                for (int x = 0; x < w; x++)
                {
                    walls[x][y] = true;
                }
            }
            //
            var action = new GetWalls(walls);
            maze.forEachEdge(action);
            backgroundimage = createImage(size.width, size.height);
            var backgroundgraphics = backgroundimage.getGraphics();
            //
            try
            {
                backgroundgraphics.setColor(Color.BLACK);
                //
                for (int x = 0; x < w; x++)
                {
                    for (int y = 0; y < h; y++)
                    {
                        if (walls[x][y]) backgroundgraphics.fillRect(x * scale, y * scale, scale, scale);
                    }
                }
            }
            finally
            {
                backgroundgraphics.dispose();
            }
        }
        //
        if (bufferimage == null) bufferimage = createImage(size.width, size.height);
        //
        long when = System.currentTimeMillis() + DELAY;
        var buffergraphics = (Graphics2D) bufferimage.getGraphics();
        buffergraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //
        try
        {
            buffergraphics.drawImage(backgroundimage, 0, 0, this);
            ghost.paintAt(buffergraphics, when);
        }
        finally
        {
            buffergraphics.dispose();
        }
        //
        long now = System.currentTimeMillis();
        long delta = when - now;
        //
        if (delta > 0L)
        {
            try
            {
                Thread.sleep(delta);
            }
            catch (InterruptedException e)
            {
            }
        }
        //
        var bounds = graphics.getClipBounds();
        graphics.drawImage(bufferimage, bounds.x, bounds.x, bounds.width, bounds.height, bounds.x, bounds.x, bounds.width, bounds.height, this);
    }
}