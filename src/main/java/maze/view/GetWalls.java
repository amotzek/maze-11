/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.view;
/*
 * Created on 23.01.2005
 */
import maze.model.Edge;
import maze.model.EdgeAction;
import maze.model.Place;
/**
 * @author Andreasm
 */
final class GetWalls implements EdgeAction
{
    private final boolean[][] walls;
    //
    public GetWalls(boolean[][] walls)
    {
        super();
        //
        this.walls = walls;
    }
    //
    public void invokeOn(Edge edge)
    {
        var from = edge.getFrom();
        var to = edge.getTo();
        int x = 1 + from.getX() + to.getX();
        int y = 1 + from.getY() + to.getY();
        walls[x][y] = false;
    }
}