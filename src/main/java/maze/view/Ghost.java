/*
 * Copyright (C) 2005 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.view;
//
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import maze.infrastructure.Context;
import maze.infrastructure.MovingPicture;
import maze.infrastructure.Orientation;
/**
 * Erstellungsdatum: (28.05.2002 23:37:03)
 */
public final class Ghost extends MovingPicture<Ghost> implements Orientation
{
    public static final int SIZE1 = 24;
    private static final int SIZE2 = 12;
    private static final int SIZE4 = 6;
    private static final int SIZE8 = 3;
    private static final int SIZE16 = 1;
    //
    private static final Dimension DIMENSION = new Dimension(SIZE1, SIZE1);
    //
    public static final Color BLINKY = new Color(255, 0, 0);
    public static final Color INKY = new Color(0, 255, 214);
    public static final Color PINKY = new Color(255, 173, 214);
    public static final Color CLYDE = new Color(255, 173, 49);
    //
    private final Color color;
    private int orientation;
    //
    public Ghost(Context context, Color color, int orientation)
    {
        super(context);
        //
        this.color = color;
        this.orientation = orientation;
        //
        setTransparentColor(Color.WHITE);
    }
    /*
     * @see gaming.pictures.Picture#getSize()
     */
    public Dimension getSize()
    {
        return DIMENSION;
    }
    /*
     * @see gaming.pictures.Picture#paint(java.awt.Graphics)
     */
    public void paint(Graphics graphics)
    {
        final int x = 0;
        final int y = 0;
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, SIZE1, SIZE1);
        graphics.setColor(color);
        graphics.fillOval(x, y, SIZE1, SIZE1);
        graphics.fillRect(x, y + SIZE2 - SIZE8, SIZE1, SIZE1 - SIZE2 + SIZE8);
        //
        switch (orientation)
        {
            case LEFT :
                paintEye(graphics, x + SIZE8, y + SIZE8);
                paintEye(graphics, x + SIZE2, y + SIZE8);
                break;
            //
            case RIGHT :
                paintEye(graphics, x + SIZE1 - SIZE4 - SIZE8 - SIZE16, y + SIZE8);
                paintEye(graphics, x + SIZE1 - SIZE4 - SIZE2 - SIZE16, y + SIZE8);
                break;
            //
            case UP :
                paintEye(graphics, x + SIZE8, y + SIZE16);
                paintEye(graphics, x + SIZE2 + SIZE16, y + SIZE16);
                break;
            //
            case DOWN :
                paintEye(graphics, x + SIZE8, y + SIZE4);
                paintEye(graphics, x + SIZE2 + SIZE16, y + SIZE4);
                break;
        }
    }
    //
    private void paintEye(Graphics graphics, int x, int y)
    {
        graphics.setColor(Color.WHITE);
        graphics.fillOval(x, y, SIZE4 + SIZE16, SIZE2 - SIZE8 - SIZE16);
        graphics.setColor(Color.BLUE);
        //
        switch (orientation)
        {
            case LEFT :
                graphics.fillRect(x, y + SIZE8 + SIZE16, SIZE8, SIZE8);
                break;
            //
            case RIGHT :
                graphics.fillRect(x + SIZE4 + SIZE16 - SIZE8, y + SIZE8 + SIZE16, SIZE8, SIZE8);
                break;
            //
            case UP :
                graphics.fillRect(x + SIZE8, y, SIZE8, SIZE8);
                break;
            //
            case DOWN :
                graphics.fillRect(x + SIZE8, y + SIZE2 - SIZE4, SIZE8, SIZE8);
                break;
        }
    }
    //
    public void setOrientation(int neworientation)
    {
        if (orientation != neworientation)
        {
            orientation = neworientation;
            discardSnapshot();
        }
    }
}