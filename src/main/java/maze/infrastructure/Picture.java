/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.infrastructure;
//
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.FilteredImageSource;
/**
 * Erstellungsdatum: (30.05.2002 02:14:20)
 * 
 * @author Andreasm
 */
public abstract class Picture
{
    protected final Context context;
    private Image snapshot;
    private Color transparent;
    private Point reusepoint;
    //
    public Picture(Context context)
    {
        super();
        //
        this.context = context;
    }
    //
    public final void setTransparentColor(final Color transparent)
    {
        this.transparent = transparent;
    }
    //
    public abstract Dimension getSize();
    //
    private void makeSnapshot(final Dimension size)
    {
        var image = context.createImage(size.width, size.height);
        var graphics = image.getGraphics();
        paint(graphics);
        graphics.dispose();
        //
        if (transparent == null)
        {
            snapshot = image;
            //
            return;
        }
        //
        var filter = new TransparencyFilter(transparent);
        var producer = new FilteredImageSource(image.getSource(), filter);
        snapshot = context.createImage(producer);
    }
    //
    public final void discardSnapshot()
    {
        if (snapshot != null) snapshot.flush();
        //
        snapshot = null;
    }
    //
    public abstract void paint(Graphics graphics);
    //
    public final void paintSnapshot(final Graphics graphics, final Rectangle rect)
    {
        var size = getSize();
        //
        synchronized (this)
        {
            if (snapshot == null) makeSnapshot(size);
        }
        //
        if (rect == null)
        {
            graphics.drawImage(snapshot, 0, 0, context);
            //
            return;
        }
        //
        reusepoint = center(size.width, size.height, rect, reusepoint);
        graphics.drawImage(snapshot, reusepoint.x, reusepoint.y, context);
    }
    //
    public static Point center(final int w, final int h, final Rectangle rect, final Point point)
    {
        final int dw = (rect.width - w) >> 1;
        final int dh = (rect.height - h) >> 1;
        //
        if (point == null) return new Point(rect.x + dw, rect.y + dh);
        //
        point.x = rect.x + dw;
        point.y = rect.y + dh;
        //
        return point;
    }
}