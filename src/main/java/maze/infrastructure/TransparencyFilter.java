/*
 * Copyright (C) 2005 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.infrastructure;
//
import java.awt.Color;
import java.awt.image.RGBImageFilter;
/**
 * Erstellungsdatum: (31.05.2002 21:54:53)
 * 
 * @author Administrator
 */
final class TransparencyFilter extends RGBImageFilter
{
    private final int transparent;
    //
    public TransparencyFilter(Color color)
    {
        super();
        //
        canFilterIndexColorModel = true;
        transparent = color.getRGB() | 0xff000000;
    }
    //
    public int filterRGB(final int x, final int y, final int rgb)
    {
        if ((rgb | 0xff000000) == transparent) return 0x00ffffff & rgb;
        //
        return rgb;
    }
}