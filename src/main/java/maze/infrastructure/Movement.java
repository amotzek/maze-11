/*
 * Copyright (C) 2005 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.infrastructure;
//
import java.awt.Rectangle;
/**
 * Erstellungsdatum: (30.05.2002 02:27:04)
 * 
 * @author Andreasm
 */
public final class Movement<P> implements Orientation
{
    private final Rectangle from;
    private final Rectangle to;
    private final int orientation;
    private final int distance;
    private final long start;
    private final int velocity;
    private MovementEventHandler<P> handler;
    private boolean ended;
    //
    public Movement(Rectangle from, Rectangle to, long start, int velocity)
    {
        super();
        //
        this.from = from;
        this.to = to;
        this.start = start;
        this.velocity = velocity;
        //
        if (from.x == to.x)
        {
            // vertical
            if (from.y < to.y)
            {
                orientation = DOWN;
                distance = to.y - from.y;
            }
            else if (from.y > to.y)
            {
                orientation = UP;
                distance = from.y - to.y;
            }
            else
            {
                throw new IllegalArgumentException();
            }
        }
        else if (from.y == to.y)
        {
            // horizontal
            if (from.x < to.x)
            {
                orientation = RIGHT;
                distance = to.x - from.x;
            }
            else
            {
                orientation = LEFT;
                distance = from.x - to.x;
            }
        }
        else
        {
            throw new IllegalArgumentException();
        }
    }
    //
    public int getOrientation()
    {
        return orientation;
    }
    //
    public Rectangle getTo()
    {
        return to;
    }
    //
    public boolean isHorizontal()
    {
        return from.y == to.y;
    }
    //
    public boolean isVertical()
    {
        return from.x == to.x;
    }
    //
    public final void setEventHandler(final MovementEventHandler<P> handler)
    {
        this.handler = handler;
    }
    //
    public Rectangle getPixelPositionAt(final P picture, final long when, final Rectangle rect)
    {
        if (when < start) return new Rectangle(from.x, from.y, from.width, from.height);
        //
        final long dt = (when - start);
        final long ds = dt * velocity;
        final long r = ds / distance;
        //
        if (r > 1024L)
        {
            notifyHandler(picture);
            //
            return new Rectangle(to.x, to.y, to.width, to.height);
        }
        //
        final long s = 1024L - r;
        final int x = (int) ((s * from.x + r * to.x) >> 10);
        final int y = (int) ((s * from.y + r * to.y) >> 10);
        //
        if (rect == null) return new Rectangle(x, y, from.width, from.height);
        //
        rect.x = x;
        rect.y = y;
        rect.width = from.width;
        rect.height = from.height;
        //
        return rect;
    }
    //
	private synchronized void notifyHandler(final P picture)
    {
        if (handler == null) return;
        if (ended) return;
        //
        ended = true;
        handler.movementEnded(this, picture);
    }
}