/*
 * Copyright (C) 2005 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.infrastructure;
//
import java.awt.Graphics;
import java.awt.Rectangle;
/**
 * Erstellungsdatum: (30.05.2002 02:17:01)
 * 
 * @author Andreasm
 */
public abstract class MovingPicture<P> extends Picture
{
    private Movement<P> movement;
    private Rectangle reuserect;
    //
    public MovingPicture(Context context)
    {
        super(context);
    }
    //
    @SuppressWarnings("unchecked")
	public synchronized final void paintAt(final Graphics graphics, final long when)
    {
        if (movement == null) return;
        //
        reuserect = movement.getPixelPositionAt((P) this, when, reuserect);
        paintSnapshot(graphics, reuserect);
    }
    //
    public synchronized final void setMovement(final Movement<P> movement)
    {
        this.movement = movement;
    }
}