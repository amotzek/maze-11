/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.infrastructure;
/*
 * Created on 25.01.2005
 */
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
/**
 * @author Andreasm
 */
public interface Context extends ImageObserver
{
    Image createImage(int width, int height);
    //
    Image createImage(ImageProducer producer);
}