/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.infrastructure;
/**
 * Erstellungsdatum: (30.05.2002 17:05:01)
 * 
 * @author Andreasm
 */
public interface Orientation
{
    int UP = 0;
    int DOWN = 1;
    int LEFT = 2;
    int RIGHT = 3;
}