/*
 * Copyright (C) 2005 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.model;
/*
 * Created on 23.01.2005
 */
/**
 * @author Andreasm
 */
public final class Edge
{
    private final Place from;
    private final Place to;
    /**
     * Constructor for Edge
     */
    public Edge(Place from, Place to)
    {
        super();
        //
        this.from = from; 
        this.to = to;
    }
    /**
     * Method getFrom 
     *
     * @return Place
     */
    public Place getFrom()
    {
        return from;
    }
    /**
     * Method getTo 
     *
     * @return Place
     */
    public Place getTo()
    {
        return to;
    }
}