package maze.model;
/**
 * Created by andreasm on 14.11.2014
 */
public interface MazeModelListener
{
    void mazeChanged();
}