package maze.model;
//
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.LinkedList;
import lisp.List;
/**
 * Created by andreasm on 14.11.2014
 */
public final class MazeModel
{
    private final int scale;
    private final MazeBuilder builder;
    private final LinkedList<MazeModelListener> listeners;
    private Maze maze;
    private GhostMovementHandler handler;
    private boolean reverse;
    //
    public MazeModel(int width, int height, int scale) throws NoSuchProviderException, NoSuchAlgorithmException
    {
        super();
        //
        this.scale = scale;
        //
        builder = new MazeBuilder(width, height);
        listeners = new LinkedList<>();
    }
    //
    public void addListener(MazeModelListener listener)
    {
        listeners.add(listener);
    }
    //
    public void nextMaze()
    {
        var nextmaze = builder.createMaze();
        var solver = new MazeSolver(nextmaze);
        var path = solver.getPath();
        //
        if (reverse) path = List.reverse(path);
        //
        maze = nextmaze;
        handler = new GhostMovementHandler(path, scale);
        reverse = !reverse;
        //
        for (var listener : listeners)
        {
            listener.mazeChanged();
        }
    }
    //
    public Maze getMaze()
    {
        return maze;
    }
    //
    public GhostMovementHandler getHandler()
    {
        return handler;
    }
}