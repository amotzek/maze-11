/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.model;
//
import java.awt.Rectangle;
import java.util.logging.Logger;
import maze.view.Ghost;
import maze.infrastructure.Movement;
import maze.infrastructure.MovementEventHandler;
import maze.infrastructure.Orientation;
import lisp.List;
import lisp.Symbol;
/**
 * Erstellungsdatum: (30.05.2002 03:20:28)
 *
 */
public final class GhostMovementHandler implements MovementEventHandler<Ghost>
{
	private static final Logger logger = Logger.getLogger("de.qrst.maze");
	//
    private Symbol from;
    private List rest;
    private boolean ended;
    private final int scale;
    //
    GhostMovementHandler(List path, int scale)
    {
        super();
        //
        this.scale = scale;
        //
        from = (Symbol) path.first();
        rest = path.rest();
    }
    /*
     * @see gaming.pictures.MovementEventHandler#movementEnded(gaming.pictures.Movement, gaming.pictures.MovingPicture)
     */
    public synchronized void movementEnded(Movement<Ghost> movement, Ghost ghost)
    {
        if (rest == null)
        {
            ended = true;
            //
            return;
        }
        //
        long start = System.currentTimeMillis() + 500L;
        var to = (Symbol) rest.first();
        rest = rest.rest();
        //
        while (rest != null)
        {
            Symbol symbol = (Symbol) rest.first();
            //
            if (!sameDirection(from, to, symbol)) break;
            //
            to = symbol;
            rest = rest.rest();
        }
        //
        var builder = new StringBuilder();
        builder.append("move from ");
        builder.append(from);
        builder.append(" to ");
        builder.append(to);
        logger.fine(builder.toString());
        var nextmovement = new Movement<Ghost>(toRectangle(from), toRectangle(to), start, 15);
        from = to;
        nextmovement.setEventHandler(this);
        ghost.setOrientation(nextmovement.getOrientation());
        ghost.setMovement(nextmovement);
    }
    //
    public synchronized boolean endOfPath()
    {
        return ended;
    }
    //
    private Rectangle toRectangle(Symbol symbol)
    {
        var name = symbol.getName();
        int pos = name.indexOf(',');
        int x = Integer.parseInt(name.substring(0, pos));
        int y = Integer.parseInt(name.substring(++pos));
        x *= 2;
        y *= 2;
        x++;
        y++;
        x *= scale;
        y *= scale;
        x -= 3;
        y -= 3;
        //
        return new Rectangle(x, y, Ghost.SIZE1, Ghost.SIZE1);
    }
    //
    private boolean sameDirection(Symbol point1, Symbol point2, Symbol point3)
    {
        return getDirection(point1, point2) == getDirection(point2, point3);
    }
    //
    private int getDirection(Symbol point1, Symbol point2)
    {
        var rectangle1 = toRectangle(point1);
        var rectangle2 = toRectangle(point2);
        int dx = rectangle1.x - rectangle2.x;
        int dy = rectangle1.y - rectangle2.y;
        //
        if (dx > 0) return Orientation.RIGHT;
        if (dx < 0) return Orientation.LEFT;
        if (dy > 0) return Orientation.DOWN;
        //
        return Orientation.UP;
    }
}