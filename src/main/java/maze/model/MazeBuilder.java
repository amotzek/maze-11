/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.model;
/*
 * Created on 23.01.2005
 */
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.Random;
/**
 * @author Andreasm
 */
public final class MazeBuilder
{
    private final int width;
    private final int height;
    private final Random random;
    private final Place[] places;
    private final Edge[] edges;
    /**
     * Constructor for MazeBuilder
     * 
     * @throws NoSuchProviderException
     * @throws NoSuchAlgorithmException
     */
    public MazeBuilder(int width, int height) throws NoSuchAlgorithmException, NoSuchProviderException
    {
        super();
        //
        this.width = width;
        this.height = height;
        //
        random = SecureRandom.getInstance("SHA1PRNG", "SUN");
        var placematrix = new Place[width][height];
        var placequeue = new LinkedList<Place>();
        var edgequeue = new LinkedList<Edge>();
        //
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                var place = new Place(x, y);
                placematrix[x][y] = place;
                placequeue.addLast(place);
            }
        }
        //
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                var from = placematrix[x][y];
                //
                if (x > 0) edgequeue.addLast(new Edge(from, placematrix[x - 1][y]));
                if (x < width - 1) edgequeue.addLast(new Edge(from, placematrix[x + 1][y]));
                if (y > 0) edgequeue.addLast(new Edge(from, placematrix[x][y - 1]));
                if (y < height - 1) edgequeue.addLast(new Edge(from, placematrix[x][y + 1]));
            }
        }
        //
        places = new Place[placequeue.size()];
        placequeue.toArray(places);
        edges = new Edge[edgequeue.size()];
        edgequeue.toArray(edges);
    }
    /**
     * Method createMaze 
     *
     * @return Maze
     */
    public synchronized Maze createMaze()
    {
        for (var place : places)
        {
            place.setConnected(null);
        }
        //
        for (int i = 0; i < edges.length; i++)
        {
            int j = random.nextInt(edges.length);
            //
            if (i == j) continue;
            //
            var edge = edges[i];
            edges[i] = edges[j];
            edges[j] = edge;
        }
        //
        var maze = new Maze(width, height);
        int components = places.length;
        int next = 0;
        //
        while (components > 1)
        {
            if (next >= edges.length) return null;
            //
            var edge = edges[next++];
            var from = edge.getFrom();
            var to = edge.getTo();
            //
            if (find(from) == find(to)) continue;
            //
            union(from, to);
            maze.addEdge(edge);
            components--;
        }
        //
        return maze;
    }
    /**
     * Method find 
     *
     * @param place
     * @return Place
     */
    private Place find(Place place)
    {
        var placestack = new LinkedList<Place>();
        //
        while (place != null)
        {
        	placestack.addLast(place);
            place = place.getConnected();
        }
        //
    	var parent = placestack.removeLast();
    	//
    	while (!placestack.isEmpty())
        {
        	var child = placestack.removeLast();
        	child.setConnected(parent);
        }
        //
        return parent;
    }
    /**
     * Method union 
     *
     * @param place1
     * @param place2
     */
    private void union(Place place1, Place place2)
    {
        var parent1 = find(place1);
        parent1.setConnected(place2);
    }
}