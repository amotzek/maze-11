/*
 * Copyright (C) 2005 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.model;
/*
 * Created on 23.01.2005
 */
/**
 * @author Andreasm
 */
public final class Place
{
    private final int x;
    private final int y;
    private Place connected;
    /**
     * Constructor for Place
     */
    public Place(int x, int y)
    {
        super();
        //
        this.x = x;
        this.y = y;
    }
    /**
     * Method getX 
     *
     * @return int
     */
    public int getX()
    {
        return x;
    }
    /**
     * Method getY 
     *
     * @return  int
     */
    public int getY()
    {
        return y;
    }
    /**
     * Method getConnected 
     *
     * @return Place
     */
    Place getConnected()
    {
        return connected;
    }
    /**
     * Method setConnected 
     *
     * @param connected
     */
    void setConnected(Place connected)
    {
        this.connected = connected;
    }
}