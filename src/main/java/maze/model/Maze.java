/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.model;
//
import java.util.LinkedList;
/*
 * Created on 23.01.2005
 */
/**
 * @author Andreasm
 */
public final class Maze
{
    private final int width;
    private final int height;
    private final LinkedList<Edge> edges;
    /**
     * Constructor for Maze
     */
    Maze(int width, int height)
    {
        super();
        //
        this.width = width;
        this.height = height;
        //
        edges = new LinkedList<Edge>();
    }
    /**
     * Method addEdge 
     *
     * @param edge
     */
    void addEdge(Edge edge)
    {
        edges.addLast(edge);
    }
    /**
     * Method getHeight 
     *
     * @return int
     */
    public int getHeight()
    {
        return height;
    }
    /**
     * Method getWidth 
     *
     * @return int
     */
    public int getWidth()
    {
        return width;
    }
    /**
     * Method forEachEdge 
     *
     * @param action
     */
    public void forEachEdge(EdgeAction action)
    {
    	for (var edge : edges)
    	{
    		action.invokeOn(edge);
    	}
    }
}