/*
 * Copyright (C) 2005, 2020 Andreas Motzek andreasm@qrst.de
 * 
 * This file is part of the Maze package.
 * 
 * You can use, redistribute and/or modify this file 
 * under the terms of the Modified Artistic License.
 * See http://www.qrst.de/wiki/license1.html for details.
 * 
 * This file is distributed in the hope that it will
 * be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness
 * for a particular purpose.
 */
package maze.model;
/*
 * Created on 24.01.2005
 */
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import lisp.Closure;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.combinator.EnvironmentFactory;
import lisp.environment.io.EnvironmentReader;
/**
 * @author andreasm
 */
final class MazeSolver implements EdgeAction
{
	private static final Logger logger = Logger.getLogger("de.qrst.maze");
	//
    private final Maze maze;
    private final HashMap<Symbol, List> edges;
    //
    public MazeSolver(Maze maze)
    {
        super();
        //
        this.maze = maze;
        //
        edges = new HashMap<Symbol, List>();
    }
    //
    public List getPath()
    {
        maze.forEachEdge(this);
        var keys = edges.keySet();
        List structure = null;
        //
        for (var from : keys)
        {
            var to = edges.get(from);
            structure = new List(new List(from, to), structure);
        }
        //
        edges.clear();
        int width = maze.getWidth();
        int height = maze.getHeight();
        var code = new List(quote(toSymbol(width - 1, height - 1)), null);
        code = new List(quote(toSymbol(0, 0)), code);
        code = new List(quote(structure), code);
        code = new List(Symbol.createSymbol("solve-maze"), code);
        logger.info("eval " + code);
        //
        try
        {
            var env = EnvironmentFactory.createEnvironment();
            var envreader = new EnvironmentReader(env);
            envreader.readFromResource(getClass(), "solve-maze.lisp", "UTF-8");
            var closure = new Closure(env, code);
            //
            return (List) closure.eval();
        }
        catch (Exception e)
        {
            logger.log(Level.WARNING, "cannot eval", e);
        }
        //
        return null;
    }
    //
    public void invokeOn(Edge edge)
    {
        var from = edge.getFrom();
        var to = edge.getTo();
        addEdge(from, to);
        addEdge(to, from);
    }
    //
    private void addEdge(Place fromplace, Place toplace)
    {
        var fromsymbol = toSymbol(fromplace);
        var tosymbol = toSymbol(toplace);
        var list = edges.get(fromsymbol);
        list = new List(tosymbol, list);
        edges.put(fromsymbol, list);
    }
    //
    private static Symbol toSymbol(Place place)
    {
        return toSymbol(place.getX(), place.getY());
    }
    //
    private static Symbol toSymbol(int x, int y)
    {
        var builder = new StringBuilder();
        builder.append(x);
        builder.append(",");
        builder.append(y);
        //
        return Symbol.createSymbol(builder.toString());
    }
    //
    private static List quote(Sexpression sexpr)
    {
        var list = new List(sexpr, null);
        list = new List(Symbol.createSymbol("quote"), list);
        //
        return list;
    }
}