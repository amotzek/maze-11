package maze.controller;
//
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.Timer;
import maze.model.MazeModel;
import maze.view.MazePanel;
/**
 * Created by andreasm on 14.11.2014
 */
public final class MazeController implements ActionListener, WindowListener
{
    private final MazeModel model;
    private final JFrame frame;
    private final MazePanel panel;
    private Timer timer;
    //
    public MazeController(MazeModel model, JFrame frame, MazePanel panel)
    {
        this.model = model;
        this.frame = frame;
        this.panel = panel;
    }
    //
    public void connect()
    {
        frame.addWindowListener(this);
        timer = new Timer(100, this);
        timer.setRepeats(true);
        timer.start();
    }
    //
    public void actionPerformed(ActionEvent event)
    {
        var handler = model.getHandler();
        //
        if (handler == null || handler.endOfPath())
        {
            model.nextMaze();
        }
        else
        {
            panel.repaint();
        }
    }
    //
    public void windowOpened(WindowEvent event)
    {
    }
    //
    public void windowClosing(WindowEvent event)
    {
        frame.dispose();
        timer.stop();
    }
    //
    public void windowClosed(WindowEvent event)
    {
    }
    //
    public void windowIconified(WindowEvent event)
    {
    }
    //
    public void windowDeiconified(WindowEvent event)
    {
    }
    //
    public void windowActivated(WindowEvent event)
    {
    }
    //
    public void windowDeactivated(WindowEvent event)
    {
    }
}